#include<stdio.h>
#include<stdlib.h>
#include<time.h>

/* STRUCT TREE NODE*/
struct treeNode{
     struct treeNode *leftPtr;
     int data;
    struct treeNode *rightPtr;
};
typedef struct treeNode TREENODE;
typedef TREENODE *TREENODEPTR;

/*PROTOTIPOS DE FUNCOES*/
void insertNode(TREENODEPTR *, int);
void inOrder(TREENODEPTR);
void preOrder(TREENODEPTR);
void postOrder(TREENODEPTR);

/*MAIN*/
int main(void){

  register int i;
  int item;   
  
TREENODEPTR rootPtr = NULL;
   srand(time(NULL));
  
    printf("Os numeros que estao sendo colocado na arvore sao: ");
  
      for(i = 1; i<=10 ;  i++){
        item = rand()%15;
          printf("%3d" , item);
          insertNode(&rootPtr , item);
    }
  
     printf("\n\nO percurso com o preOrder e: ");
     preOrder(rootPtr);

     printf("\n\nO percurso com inOrden e: ");
     inOrder(rootPtr);

     printf("\n\nO percurso com postOrder e:  ");
     postOrder(rootPtr);
     

 return 0;
}
void insertNode(TREENODEPTR *treePtr, int value){
if(*treePtr == NULL) { /* *treePtr tem valor NULL */
       *treePtr = malloc(sizeof(TREENODE));

       if(*treePtr != NULL) {

         (*treePtr)-> data = value;
         (*treePtr)-> leftPtr = NULL;
         (*treePtr)-> rightPtr = NULL;
     }
       else{
        printf ( "%d Nao inserido. Nao existe memória disponivel. \n", value);
   }
    }else{
     if(value < (*treePtr)->data)
      insertNode(&((*treePtr)->leftPtr), value);
     else{
      
      if(value >= (*treePtr)->data)
         insertNode(&((*treePtr)->rightPtr), value);
          else
          printf("dup");
    }
  }
}

void inOrder(TREENODEPTR treePtr){
     if(treePtr != NULL){
        inOrder(treePtr->leftPtr);
        printf("%3d", treePtr->rightPtr);

   }
}

void preOrder(TREENODEPTR treePtr){
     if(treePtr != NULL){
        printf("%3d", treePtr->data);
        preOrder(treePtr->leftPtr);
        preOrder(treePtr->rightPtr);
   }
}

void postOrder(TREENODEPTR treePtr){
      if(treePtr != NULL){
        postOrder(treePtr->leftPtr);
        postOrder(treePtr->rightPtr);
        printf("%3d ",treePtr->data);
   }
}